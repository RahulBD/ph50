#include <stdio.h>
int swap_ref(int *,int *);
int main()
{
    int c,d;
    printf("enter two numbers to swap");
    scanf("%d%d",&c,&d);
    swap_ref(&c,&d);
    return 0;
}
int swap_ref(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
    printf("number after swapping=%d %d",*a,*b);
}